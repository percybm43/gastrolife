const app = Vue.createApp({
    data() {
        return {
            title: "Recetas deliciosas y faciles de hacer",
            description: "A Recipe Collection For Every Palate",
            image: "#",
            likes: 15,

            categories: [
                { id: 1, name: "All" },
                { id: 2, name: "Desayuno" },
                { id: 3, name: "Bebidas" },
                { id: 4, name: "Entradas" },
                { id: 5, name: "Almuerzo" },
                { id: 6, name: "Postres" },
                { id: 7, name: "Sopas" }
            ],

            all_recipes: [],
            selectedIndex: 0,
            hasRecipes: true,
            top_recipes: [
                { id: 1, image: "./imgs/recipe-2oXOqgugPi.png", name: "spaguetti", category: "Lunch", time: "20 mins", level: "Easy", likes: 18, ingredients: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. ", instructions: "Morbi sodales ac purus et pretium. Morbi a dui at nulla pulvinar sodales. Aliquam ac felis ut felis suscipit finibus vitae vitae justo. Aliquam et arcu non felis consequat faucibus in nec dolor. Vivamus eget cursus velit. Morbi in felis a erat egestas iaculis gravida vel ex. Sed non nibh sed enim cursus commodo. Fusce fermentum venenatis orci at tempor. Etiam vitae metus a tellus viverra varius id ac nisl. Proin velit orci, molestie a maximus sit amet, molestie rutrum nulla. Aliquam eget lectus quis enim porta aliquam vitae sit amet tellus. Etiam porttitor eu felis et scelerisque. Aenean finibus placerat viverra. Nunc vel justo quis metus suscipit gravida eu non mi. In hac habitasse platea dictumst. Mauris semper laoreet dictum." },
                { id: 2, image: "./imgs/recipe-2Vxz5w1oju.png", name: "Fettuccine Alfredo", category: "Dinner", time: "20 mins", level: "Easy", likes: 18, ingredients: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.", instructions: "Morbi sodales ac purus et pretium. Morbi a dui at nulla pulvinar sodales. Aliquam ac felis ut felis suscipit finibus vitae vitae justo. Aliquam et arcu non felis consequat faucibus in nec dolor. Vivamus eget cursus velit. Morbi in felis a erat egestas iaculis gravida vel ex. Sed non nibh sed enim cursus commodo. Fusce fermentum venenatis orci at tempor. Etiam vitae metus a tellus viverra varius id ac nisl. Proin velit orci, molestie a maximus sit amet, molestie rutrum nulla. Aliquam eget lectus quis enim porta aliquam vitae sit amet tellus. Etiam porttitor eu felis et scelerisque. Aenean finibus placerat viverra. Nunc vel justo quis metus suscipit gravida eu non mi. In hac habitasse platea dictumst. Mauris semper laoreet dictum." },
                { id: 3, image: "./imgs/recipe-2oXOqgugPi.png", name: "spaguetti", category: "Lunch", time: "20 mins", level: "Easy", likes: 18, ingredients: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. ", instructions: "Morbi sodales ac purus et pretium. Morbi a dui at nulla pulvinar sodales. Aliquam ac felis ut felis suscipit finibus vitae vitae justo. Aliquam et arcu non felis consequat faucibus in nec dolor. Vivamus eget cursus velit. Morbi in felis a erat egestas iaculis gravida vel ex. Sed non nibh sed enim cursus commodo. Fusce fermentum venenatis orci at tempor. Etiam vitae metus a tellus viverra varius id ac nisl. Proin velit orci, molestie a maximus sit amet, molestie rutrum nulla. Aliquam eget lectus quis enim porta aliquam vitae sit amet tellus. Etiam porttitor eu felis et scelerisque. Aenean finibus placerat viverra. Nunc vel justo quis metus suscipit gravida eu non mi. In hac habitasse platea dictumst. Mauris semper laoreet dictum." },
                { id: 4, image: "./imgs/recipe-2Vxz5w1oju.png", name: "Fettuccine Alfredo", category: "Dinner", time: "20 mins", level: "Easy", likes: 18, ingredients: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.", instructions: "Morbi sodales ac purus et pretium. Morbi a dui at nulla pulvinar sodales. Aliquam ac felis ut felis suscipit finibus vitae vitae justo. Aliquam et arcu non felis consequat faucibus in nec dolor. Vivamus eget cursus velit. Morbi in felis a erat egestas iaculis gravida vel ex. Sed non nibh sed enim cursus commodo. Fusce fermentum venenatis orci at tempor. Etiam vitae metus a tellus viverra varius id ac nisl. Proin velit orci, molestie a maximus sit amet, molestie rutrum nulla. Aliquam eget lectus quis enim porta aliquam vitae sit amet tellus. Etiam porttitor eu felis et scelerisque. Aenean finibus placerat viverra. Nunc vel justo quis metus suscipit gravida eu non mi. In hac habitasse platea dictumst. Mauris semper laoreet dictum." },
                { id: 5, image: "./imgs/recipe-7hBTce0s9e.png", name: "cookies", category: "Breakfast", time: "20 mins", level: "Easy", likes: 18, ingredients: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.", instructions: "Morbi sodales ac purus et pretium. Morbi a dui at nulla pulvinar sodales. Aliquam ac felis ut felis suscipit finibus vitae vitae justo. Aliquam et arcu non felis consequat faucibus in nec dolor. Vivamus eget cursus velit. Morbi in felis a erat egestas iaculis gravida vel ex. Sed non nibh sed enim cursus commodo. Fusce fermentum venenatis orci at tempor. Etiam vitae metus a tellus viverra varius id ac nisl. Proin velit orci, molestie a maximus sit amet, molestie rutrum nulla. Aliquam eget lectus quis enim porta aliquam vitae sit amet tellus. Etiam porttitor eu felis et scelerisque. Aenean finibus placerat viverra. Nunc vel justo quis metus suscipit gravida eu non mi. In hac habitasse platea dictumst. Mauris semper laoreet dictum." },
                { id: 6, image: "./imgs/recipe-bjbBc1eYt4.png", name: "pasta", category: "Desserts", time: "20 mins", level: "Easy", likes: 18, ingredients: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.", instructions: "Morbi sodales ac purus et pretium. Morbi a dui at nulla pulvinar sodales. Aliquam ac felis ut felis suscipit finibus vitae vitae justo. Aliquam et arcu non felis consequat faucibus in nec dolor. Vivamus eget cursus velit. Morbi in felis a erat egestas iaculis gravida vel ex. Sed non nibh sed enim cursus commodo. Fusce fermentum venenatis orci at tempor. Etiam vitae metus a tellus viverra varius id ac nisl. Proin velit orci, molestie a maximus sit amet, molestie rutrum nulla. Aliquam eget lectus quis enim porta aliquam vitae sit amet tellus. Etiam porttitor eu felis et scelerisque. Aenean finibus placerat viverra. Nunc vel justo quis metus suscipit gravida eu non mi. In hac habitasse platea dictumst. Mauris semper laoreet dictum." },
                { id: 7, image: "./imgs/recipe-akeB3kZJog.png", name: "pasta", category: "Drinks", time: "20 mins", level: "Easy", likes: 18, ingredients: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.", instructions: "Morbi sodales ac purus et pretium. Morbi a dui at nulla pulvinar sodales. Aliquam ac felis ut felis suscipit finibus vitae vitae justo. Aliquam et arcu non felis consequat faucibus in nec dolor. Vivamus eget cursus velit. Morbi in felis a erat egestas iaculis gravida vel ex. Sed non nibh sed enim cursus commodo. Fusce fermentum venenatis orci at tempor. Etiam vitae metus a tellus viverra varius id ac nisl. Proin velit orci, molestie a maximus sit amet, molestie rutrum nulla. Aliquam eget lectus quis enim porta aliquam vitae sit amet tellus. Etiam porttitor eu felis et scelerisque. Aenean finibus placerat viverra. Nunc vel justo quis metus suscipit gravida eu non mi. In hac habitasse platea dictumst. Mauris semper laoreet dictum." },
                { id: 8, image: "./imgs/recipe-Bcd80EFpkx.png", name: "Strawberry Lemonade", category: "Drinks", time: "20 mins", level: "Easy", likes: 18, ingredients: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.", instructions: "Morbi sodales ac purus et pretium. Morbi a dui at nulla pulvinar sodales. Aliquam ac felis ut felis suscipit finibus vitae vitae justo. Aliquam et arcu non felis consequat faucibus in nec dolor. Vivamus eget cursus velit. Morbi in felis a erat egestas iaculis gravida vel ex. Sed non nibh sed enim cursus commodo. Fusce fermentum venenatis orci at tempor. Etiam vitae metus a tellus viverra varius id ac nisl. Proin velit orci, molestie a maximus sit amet, molestie rutrum nulla. Aliquam eget lectus quis enim porta aliquam vitae sit amet tellus. Etiam porttitor eu felis et scelerisque. Aenean finibus placerat viverra. Nunc vel justo quis metus suscipit gravida eu non mi. In hac habitasse platea dictumst. Mauris semper laoreet dictum." },
                { id: 9, image: "./imgs/recipe-COmT7S9oDS.png", name: "Sushi", category: "Lunch", time: "20 mins", level: "Easy", likes: 18, ingredients: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.", instructions: "Morbi sodales ac purus et pretium. Morbi a dui at nulla pulvinar sodales. Aliquam ac felis ut felis suscipit finibus vitae vitae justo. Aliquam et arcu non felis consequat faucibus in nec dolor. Vivamus eget cursus velit. Morbi in felis a erat egestas iaculis gravida vel ex. Sed non nibh sed enim cursus commodo. Fusce fermentum venenatis orci at tempor. Etiam vitae metus a tellus viverra varius id ac nisl. Proin velit orci, molestie a maximus sit amet, molestie rutrum nulla. Aliquam eget lectus quis enim porta aliquam vitae sit amet tellus. Etiam porttitor eu felis et scelerisque. Aenean finibus placerat viverra. Nunc vel justo quis metus suscipit gravida eu non mi. In hac habitasse platea dictumst. Mauris semper laoreet dictum." },
                { id: 10, image: "./imgs/recipe-cPZ4QUw0OM.png", name: "Fettuccine Alfredo", category: "Dinner", time: "20 mins", level: "Easy", likes: 18, ingredients: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.", instructions: "Morbi sodales ac purus et pretium. Morbi a dui at nulla pulvinar sodales. Aliquam ac felis ut felis suscipit finibus vitae vitae justo. Aliquam et arcu non felis consequat faucibus in nec dolor. Vivamus eget cursus velit. Morbi in felis a erat egestas iaculis gravida vel ex. Sed non nibh sed enim cursus commodo. Fusce fermentum venenatis orci at tempor. Etiam vitae metus a tellus viverra varius id ac nisl. Proin velit orci, molestie a maximus sit amet, molestie rutrum nulla. Aliquam eget lectus quis enim porta aliquam vitae sit amet tellus. Etiam porttitor eu felis et scelerisque. Aenean finibus placerat viverra. Nunc vel justo quis metus suscipit gravida eu non mi. In hac habitasse platea dictumst. Mauris semper laoreet dictum." }
            ],

            coleccion_recipes: [
                { id: 1, image: "./imgs/recipe-2oXOqgugPi.png", name: "spaguetti", category: "Lunch", time: "20 mins", level: "Easy", likes: 18, ingredients: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. ", instructions: "Morbi sodales ac purus et pretium. Morbi a dui at nulla pulvinar sodales. Aliquam ac felis ut felis suscipit finibus vitae vitae justo. Aliquam et arcu non felis consequat faucibus in nec dolor. Vivamus eget cursus velit. Morbi in felis a erat egestas iaculis gravida vel ex. Sed non nibh sed enim cursus commodo. Fusce fermentum venenatis orci at tempor. Etiam vitae metus a tellus viverra varius id ac nisl. Proin velit orci, molestie a maximus sit amet, molestie rutrum nulla. Aliquam eget lectus quis enim porta aliquam vitae sit amet tellus. Etiam porttitor eu felis et scelerisque. Aenean finibus placerat viverra. Nunc vel justo quis metus suscipit gravida eu non mi. In hac habitasse platea dictumst. Mauris semper laoreet dictum." },
                { id: 2, image: "./imgs/recipe-2Vxz5w1oju.png", name: "Fettuccine Alfredo", category: "Dinner", time: "20 mins", level: "Easy", likes: 18, ingredients: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.", instructions: "Morbi sodales ac purus et pretium. Morbi a dui at nulla pulvinar sodales. Aliquam ac felis ut felis suscipit finibus vitae vitae justo. Aliquam et arcu non felis consequat faucibus in nec dolor. Vivamus eget cursus velit. Morbi in felis a erat egestas iaculis gravida vel ex. Sed non nibh sed enim cursus commodo. Fusce fermentum venenatis orci at tempor. Etiam vitae metus a tellus viverra varius id ac nisl. Proin velit orci, molestie a maximus sit amet, molestie rutrum nulla. Aliquam eget lectus quis enim porta aliquam vitae sit amet tellus. Etiam porttitor eu felis et scelerisque. Aenean finibus placerat viverra. Nunc vel justo quis metus suscipit gravida eu non mi. In hac habitasse platea dictumst. Mauris semper laoreet dictum." },
                { id: 3, image: "./imgs/recipe-2oXOqgugPi.png", name: "spaguetti", category: "Lunch", time: "20 mins", level: "Easy", likes: 18, ingredients: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. ", instructions: "Morbi sodales ac purus et pretium. Morbi a dui at nulla pulvinar sodales. Aliquam ac felis ut felis suscipit finibus vitae vitae justo. Aliquam et arcu non felis consequat faucibus in nec dolor. Vivamus eget cursus velit. Morbi in felis a erat egestas iaculis gravida vel ex. Sed non nibh sed enim cursus commodo. Fusce fermentum venenatis orci at tempor. Etiam vitae metus a tellus viverra varius id ac nisl. Proin velit orci, molestie a maximus sit amet, molestie rutrum nulla. Aliquam eget lectus quis enim porta aliquam vitae sit amet tellus. Etiam porttitor eu felis et scelerisque. Aenean finibus placerat viverra. Nunc vel justo quis metus suscipit gravida eu non mi. In hac habitasse platea dictumst. Mauris semper laoreet dictum." },
                { id: 4, image: "./imgs/recipe-2Vxz5w1oju.png", name: "Fettuccine Alfredo", category: "Dinner", time: "20 mins", level: "Easy", likes: 18, ingredients: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.", instructions: "Morbi sodales ac purus et pretium. Morbi a dui at nulla pulvinar sodales. Aliquam ac felis ut felis suscipit finibus vitae vitae justo. Aliquam et arcu non felis consequat faucibus in nec dolor. Vivamus eget cursus velit. Morbi in felis a erat egestas iaculis gravida vel ex. Sed non nibh sed enim cursus commodo. Fusce fermentum venenatis orci at tempor. Etiam vitae metus a tellus viverra varius id ac nisl. Proin velit orci, molestie a maximus sit amet, molestie rutrum nulla. Aliquam eget lectus quis enim porta aliquam vitae sit amet tellus. Etiam porttitor eu felis et scelerisque. Aenean finibus placerat viverra. Nunc vel justo quis metus suscipit gravida eu non mi. In hac habitasse platea dictumst. Mauris semper laoreet dictum." },
                { id: 5, image: "./imgs/recipe-7hBTce0s9e.png", name: "cookies", category: "Breakfast", time: "20 mins", level: "Easy", likes: 18, ingredients: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.", instructions: "Morbi sodales ac purus et pretium. Morbi a dui at nulla pulvinar sodales. Aliquam ac felis ut felis suscipit finibus vitae vitae justo. Aliquam et arcu non felis consequat faucibus in nec dolor. Vivamus eget cursus velit. Morbi in felis a erat egestas iaculis gravida vel ex. Sed non nibh sed enim cursus commodo. Fusce fermentum venenatis orci at tempor. Etiam vitae metus a tellus viverra varius id ac nisl. Proin velit orci, molestie a maximus sit amet, molestie rutrum nulla. Aliquam eget lectus quis enim porta aliquam vitae sit amet tellus. Etiam porttitor eu felis et scelerisque. Aenean finibus placerat viverra. Nunc vel justo quis metus suscipit gravida eu non mi. In hac habitasse platea dictumst. Mauris semper laoreet dictum." },
                { id: 6, image: "./imgs/recipe-bjbBc1eYt4.png", name: "pasta", category: "Desserts", time: "20 mins", level: "Easy", likes: 18, ingredients: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.", instructions: "Morbi sodales ac purus et pretium. Morbi a dui at nulla pulvinar sodales. Aliquam ac felis ut felis suscipit finibus vitae vitae justo. Aliquam et arcu non felis consequat faucibus in nec dolor. Vivamus eget cursus velit. Morbi in felis a erat egestas iaculis gravida vel ex. Sed non nibh sed enim cursus commodo. Fusce fermentum venenatis orci at tempor. Etiam vitae metus a tellus viverra varius id ac nisl. Proin velit orci, molestie a maximus sit amet, molestie rutrum nulla. Aliquam eget lectus quis enim porta aliquam vitae sit amet tellus. Etiam porttitor eu felis et scelerisque. Aenean finibus placerat viverra. Nunc vel justo quis metus suscipit gravida eu non mi. In hac habitasse platea dictumst. Mauris semper laoreet dictum." },
                { id: 7, image: "./imgs/recipe-akeB3kZJog.png", name: "pasta", category: "Drinks", time: "20 mins", level: "Easy", likes: 18, ingredients: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.", instructions: "Morbi sodales ac purus et pretium. Morbi a dui at nulla pulvinar sodales. Aliquam ac felis ut felis suscipit finibus vitae vitae justo. Aliquam et arcu non felis consequat faucibus in nec dolor. Vivamus eget cursus velit. Morbi in felis a erat egestas iaculis gravida vel ex. Sed non nibh sed enim cursus commodo. Fusce fermentum venenatis orci at tempor. Etiam vitae metus a tellus viverra varius id ac nisl. Proin velit orci, molestie a maximus sit amet, molestie rutrum nulla. Aliquam eget lectus quis enim porta aliquam vitae sit amet tellus. Etiam porttitor eu felis et scelerisque. Aenean finibus placerat viverra. Nunc vel justo quis metus suscipit gravida eu non mi. In hac habitasse platea dictumst. Mauris semper laoreet dictum." },
                { id: 8, image: "./imgs/recipe-Bcd80EFpkx.png", name: "Strawberry Lemonade", category: "Drinks", time: "20 mins", level: "Easy", likes: 18, ingredients: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.", instructions: "Morbi sodales ac purus et pretium. Morbi a dui at nulla pulvinar sodales. Aliquam ac felis ut felis suscipit finibus vitae vitae justo. Aliquam et arcu non felis consequat faucibus in nec dolor. Vivamus eget cursus velit. Morbi in felis a erat egestas iaculis gravida vel ex. Sed non nibh sed enim cursus commodo. Fusce fermentum venenatis orci at tempor. Etiam vitae metus a tellus viverra varius id ac nisl. Proin velit orci, molestie a maximus sit amet, molestie rutrum nulla. Aliquam eget lectus quis enim porta aliquam vitae sit amet tellus. Etiam porttitor eu felis et scelerisque. Aenean finibus placerat viverra. Nunc vel justo quis metus suscipit gravida eu non mi. In hac habitasse platea dictumst. Mauris semper laoreet dictum." },
                { id: 9, image: "./imgs/recipe-COmT7S9oDS.png", name: "Sushi", category: "Lunch", time: "20 mins", level: "Easy", likes: 18, ingredients: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.", instructions: "Morbi sodales ac purus et pretium. Morbi a dui at nulla pulvinar sodales. Aliquam ac felis ut felis suscipit finibus vitae vitae justo. Aliquam et arcu non felis consequat faucibus in nec dolor. Vivamus eget cursus velit. Morbi in felis a erat egestas iaculis gravida vel ex. Sed non nibh sed enim cursus commodo. Fusce fermentum venenatis orci at tempor. Etiam vitae metus a tellus viverra varius id ac nisl. Proin velit orci, molestie a maximus sit amet, molestie rutrum nulla. Aliquam eget lectus quis enim porta aliquam vitae sit amet tellus. Etiam porttitor eu felis et scelerisque. Aenean finibus placerat viverra. Nunc vel justo quis metus suscipit gravida eu non mi. In hac habitasse platea dictumst. Mauris semper laoreet dictum." },
                { id: 10, image: "./imgs/recipe-cPZ4QUw0OM.png", name: "Fettuccine Alfredo", category: "Dinner", time: "20 mins", level: "Easy", likes: 18, ingredients: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.", instructions: "Morbi sodales ac purus et pretium. Morbi a dui at nulla pulvinar sodales. Aliquam ac felis ut felis suscipit finibus vitae vitae justo. Aliquam et arcu non felis consequat faucibus in nec dolor. Vivamus eget cursus velit. Morbi in felis a erat egestas iaculis gravida vel ex. Sed non nibh sed enim cursus commodo. Fusce fermentum venenatis orci at tempor. Etiam vitae metus a tellus viverra varius id ac nisl. Proin velit orci, molestie a maximus sit amet, molestie rutrum nulla. Aliquam eget lectus quis enim porta aliquam vitae sit amet tellus. Etiam porttitor eu felis et scelerisque. Aenean finibus placerat viverra. Nunc vel justo quis metus suscipit gravida eu non mi. In hac habitasse platea dictumst. Mauris semper laoreet dictum." },
                { id: 11, image: "./imgs/recipe-2oXOqgugPi.png", name: "spaguetti", category: "Lunch", time: "20 mins", level: "Easy", likes: 18, ingredients: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. ", instructions: "Morbi sodales ac purus et pretium. Morbi a dui at nulla pulvinar sodales. Aliquam ac felis ut felis suscipit finibus vitae vitae justo. Aliquam et arcu non felis consequat faucibus in nec dolor. Vivamus eget cursus velit. Morbi in felis a erat egestas iaculis gravida vel ex. Sed non nibh sed enim cursus commodo. Fusce fermentum venenatis orci at tempor. Etiam vitae metus a tellus viverra varius id ac nisl. Proin velit orci, molestie a maximus sit amet, molestie rutrum nulla. Aliquam eget lectus quis enim porta aliquam vitae sit amet tellus. Etiam porttitor eu felis et scelerisque. Aenean finibus placerat viverra. Nunc vel justo quis metus suscipit gravida eu non mi. In hac habitasse platea dictumst. Mauris semper laoreet dictum." },
                { id: 12, image: "./imgs/recipe-2Vxz5w1oju.png", name: "Fettuccine Alfredo", category: "Dinner", time: "20 mins", level: "Easy", likes: 18, ingredients: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.", instructions: "Morbi sodales ac purus et pretium. Morbi a dui at nulla pulvinar sodales. Aliquam ac felis ut felis suscipit finibus vitae vitae justo. Aliquam et arcu non felis consequat faucibus in nec dolor. Vivamus eget cursus velit. Morbi in felis a erat egestas iaculis gravida vel ex. Sed non nibh sed enim cursus commodo. Fusce fermentum venenatis orci at tempor. Etiam vitae metus a tellus viverra varius id ac nisl. Proin velit orci, molestie a maximus sit amet, molestie rutrum nulla. Aliquam eget lectus quis enim porta aliquam vitae sit amet tellus. Etiam porttitor eu felis et scelerisque. Aenean finibus placerat viverra. Nunc vel justo quis metus suscipit gravida eu non mi. In hac habitasse platea dictumst. Mauris semper laoreet dictum." },
            ]
        }
    },

    mounted: function () {
        this.all_recipes = this.top_recipes;
    },
    mounted: function () {
        this.all_recipes = this.coleccion_recipes;
    },

    methods: {

        //Top recetas

        onClickLike(index) {
            //console.log("btn - click");
            this.top_recipes[index].likes += 1;
        },
        onClickUnlike(index) {
            //console.log("btn - click");
            if (this.top_recipes[index].likes > 0) this.top_recipes[index].likes -= 1;
        },
        onClickCategory(category) {

            if (category == "All") {
                this.top_recipes = this.all_recipes;
            } else {
                this.top_recipes = this.all_recipes;
                let recipesInCategory = this.top_recipes.filter(function (el) {

                    return el.category === category;
                });
                //console.log("filtered ->" + recipesInCategory.length);

                if (recipesInCategory.length > 0) {
                    this.hasRecipe = true;
                    this.top_recipes = recipesInCategory;
                } else {
                    this.hasRecipes = false;
                }
            }

        },

        onClickViewRecipe(index) {
            console.log("INDEX ->" + index + "LENGTH -> " + this.selectedIndex);
            this.selectedIndex = index;
        },
        onClickNext() {
            //console.log("INDEX ->" + this.selectedIndex)
            this.selectedIndex++;
            if (this.selectedIndex > this.top_recipes.length - 1) {
                this.selectedIndex = 0;
            }
        },
        onClickPrev() {
            this.selectedIndex--;
            if (this.selectedIndex < 0) {
                this.selectedIndex = this.top_recipes.length - 1;
            }
        },

        //Colección de recetas

        onClickLike(index) {
            //console.log("btn - click");
            this.coleccion_recipes[index].likes += 1;
        },
        onClickUnlike(index) {
            //console.log("btn - click");
            if (this.coleccion_recipes[index].likes > 0) this.coleccion_recipes[index].likes -= 1;
        },
        onClickCategory(category) {

            if (category == "All") {
                this.coleccion_recipes = this.all_recipes;
            } else {
                this.coleccion_recipes = this.all_recipes;
                let recipesInCategory = this.coleccion_recipes.filter(function (el) {

                    return el.category === category;
                });
                //console.log("filtered ->" + recipesInCategory.length);

                if (recipesInCategory.length > 0) {
                    this.hasRecipe = true;
                    this.coleccion_recipes = recipesInCategory;
                } else {
                    this.hasRecipes = false;
                }
            }

        },

        onClickNext() {
            //console.log("INDEX ->" + this.selectedIndex)
            this.selectedIndex++;
            if (this.selectedIndex > this.coleccion_recipes.length - 1) {
                this.selectedIndex = 0;
            }
        },
        onClickPrev() {
            this.selectedIndex--;
            if (this.selectedIndex < 0) {
                this.selectedIndex = this.coleccion_recipes.length - 1;
            }
        }
        //Colección de recetas
    }
});

// Menu desplegable

$(document).ready(main);

var contador = 1;

function main() {
$('.menu_bar').click(function () {
    if (contador == 1) {
        $('nav').animate({
            left: '0'
        });
        contador = 0;
    } else {
        contador = 1;
        $('nav').animate({
            left: '-100%'
        });
    }
});

// Mostramos y ocultamos submenus
$('.submenu').click(function () {
    $(this).children('.children').slideToggle();
});
}

